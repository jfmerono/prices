# Introduction
The project kairos-prices-app is build in a java artifact called practica.

* The JVM level is '17'.

# Getting Started

### Description
This service is focused on returning the current price for a product belonging to brand.

### Usage
The entrypoint consists of a REST endpoint which accepts a date with format (yyyy-MM-dd-HH.mm.ss), the identifier for the product and an identifier for the brand.
The service will return a JSON with the information of a price and its vigency.

### Example
```bash
curl -X GET 'http://localhost:8080/price?date=2020-06-14-10.00.00&product=35455&brand=1'
```
```json
{
"productId": "35455",
"brandId": "1",
"rate": "1",
"dateStart": "2020-06-14-00.00.00",
"dateEnd": "2020-12-31-23.59.59",
"price": "35.50 EUR"
}
```

### Running Tests
```
./mvnw test
```

### Running Service using installed JVM
Using local JVM installed:
```
./mvnw spring-boot:run
```
### Running Service using Docker
Building image:
```
docker build -t spring-app .
```
Creating and running container:
```
docker run -d -p 8080:8080 --name prices-app spring-app
```
Stopping and starting container:
```
docker stop prices-app
docker start prices-app
```
Alternatively, you can use docker-compose tool:
```
docker-compose up -d
```