package com.example.prices;

import com.example.prices.application.usecases.SearchPriceRequest;
import com.example.prices.infraestructure.rest.SearchPriceResponse;
import com.example.prices.application.usecases.SearchPriceUseCase;
import com.example.prices.domain.Util.Util;
import com.example.prices.domain.aggregates.Price;
import com.example.prices.domain.exceptions.PriceNotFoundException;
import com.example.prices.domain.repositories.PriceRepositoryPort;
import com.example.prices.domain.services.PriceService;
import com.example.prices.domain.vos.Amount;
import com.example.prices.domain.vos.Currency;
import com.example.prices.infraestructure.rest.mapper.SearchPriceMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SearchPriceTest {

    private final PriceRepositoryPort priceRepository = Mockito.mock(PriceRepositoryPort.class);
    private final PriceService priceService = new PriceService(priceRepository);
    private final SearchPriceUseCase useCase = new SearchPriceUseCase(priceService);

    @Test
    public void should_return_35_50_when_brandId_is_1_productId_is_35455_and_date_is_20200614_100000() {

        Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .brandId("1")
                .productId("35455")
                .date(Util.parseStringToDateTime("2020-06-14-10.00.00"))
                .build();

        SearchPriceResponse response =
                SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));

        assertEquals("35455", response.getProductId());
        assertEquals("1", response.getBrandId());
        assertEquals("1", response.getRate());
        assertEquals("35.50 EUR", response.getPrice());
        assertEquals("2020-06-14-00.00.00", response.getDateStart());
        assertEquals("2020-12-31-23.59.59", response.getDateEnd());

    }

    @Test
    public void should_return_25_45_when_brandId_is_1_productId_is_35455_and_date_is_20200614_160000() {

        Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .brandId("1")
                .productId("35455")
                .date(Util.parseStringToDateTime("2020-06-14-16.00.00"))
                .build();

        SearchPriceResponse response =
                SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));

        assertEquals("35455", response.getProductId());
        assertEquals("1", response.getBrandId());
        assertEquals("2", response.getRate());
        assertEquals("25.45 EUR", response.getPrice());
        assertEquals("2020-06-14-15.00.00", response.getDateStart());
        assertEquals("2020-06-14-18.30.00", response.getDateEnd());

    }

    @Test
    public void should_return_35_50_when_brandId_is_1_productId_is_35455_and_date_is_20200614_210000() {

        Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .brandId("1")
                .productId("35455")
                .date(Util.parseStringToDateTime("2020-06-14-21.00.00"))
                .build();

        SearchPriceResponse response =
                SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));

        assertEquals("35455", response.getProductId());
        assertEquals("1", response.getBrandId());
        assertEquals("1", response.getRate());
        assertEquals("35.50 EUR", response.getPrice());
        assertEquals("2020-06-14-00.00.00", response.getDateStart());
        assertEquals("2020-12-31-23.59.59", response.getDateEnd());

    }

    @Test
    public void should_return_30_50_when_brandId_is_1_productId_is_35455_and_date_is_20200615_100000() {

        Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .brandId("1")
                .productId("35455")
                .date(Util.parseStringToDateTime("2020-06-15-10.00.00"))
                .build();

        SearchPriceResponse response =
                SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));

        assertEquals("35455", response.getProductId());
        assertEquals("1", response.getBrandId());
        assertEquals("3", response.getRate());
        assertEquals("30.50 EUR", response.getPrice());
        assertEquals("2020-06-15-00.00.00", response.getDateStart());
        assertEquals("2020-06-15-11.00.00", response.getDateEnd());

    }

    @Test
    public void should_return_38_95_when_brandId_is_1_productId_is_35455_and_date_is_20200616_210000() {

        Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .brandId("1")
                .productId("35455")
                .date(Util.parseStringToDateTime("2020-06-16-21.00.00"))
                .build();

        SearchPriceResponse response =
                SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));

        assertEquals("35455", response.getProductId());
        assertEquals("1", response.getBrandId());
        assertEquals("4", response.getRate());
        assertEquals("38.95 EUR", response.getPrice());
        assertEquals("2020-06-15-16.00.00", response.getDateStart());
        assertEquals("2020-12-30-23.59.59", response.getDateEnd());

    }

    @Test
    public void should_throw_PriceNotFoundException_when_brandId_is_2_productId_is_35455_and_date_is_20200614_100000() {

        Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .brandId("2")
                .productId("35455")
                .date(Util.parseStringToDateTime("2020-06-14-10.00.00"))
                .build();

        Exception exception = assertThrows(PriceNotFoundException.class, () -> {
            SearchPriceResponse response =
                    SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));
        });
        assertEquals(exception.getMessage(), "Price not found");

    }

    @Test
    public void should_throw_PriceNotFoundException_when_brandId_is_1_productId_is_35555_and_date_is_20200614_100000() {

        Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .brandId("1")
                .productId("35555")
                .date(Util.parseStringToDateTime("2020-06-14-10.00.00"))
                .build();

        Exception exception = assertThrows(PriceNotFoundException.class, () -> {
            SearchPriceResponse response =
                    SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));
        });
        assertEquals(exception.getMessage(), "Price not found");

    }

    @Test
    public void should_throw_PriceNotFoundException_when_brandId_is_1_productId_is_35455_and_date_is_20200613_100000() {

        Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .brandId("1")
                .productId("35455")
                .date(Util.parseStringToDateTime("2020-06-13-10.00.00"))
                .build();

        Exception exception = assertThrows(PriceNotFoundException.class, () -> {
            SearchPriceResponse response =
                    SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));
        });
        assertEquals(exception.getMessage(), "Price not found");

    }

    @Test
    public void should_throw_PriceNotFoundException_when_brandId_is_1_productId_is_35455_and_date_is_20210101_000000() {

        Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .brandId("1")
                .productId("35455")
                .date(Util.parseStringToDateTime("2021-01-01-00.00.00"))
                .build();

        Exception exception = assertThrows(PriceNotFoundException.class, () -> {
            SearchPriceResponse response =
                    SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));
        });
        assertEquals(exception.getMessage(), "Price not found");

    }

    private List<Price> getPrices() {
        return List.of(
                Price.builder()
                        .brandId("1")
                        .priceList(1)
                        .productId("35455")
                        .priority(0)
                        .amount(new Amount(35.5))
                        .currency(Currency.valueOf("EUR"))
                        .startDate(Util.parseStringToDateTime("2020-06-14-00.00.00"))
                        .endDate(Util.parseStringToDateTime("2020-12-31-23.59.59"))
                        .build(),

                Price.builder()
                        .brandId("1")
                        .priceList(2)
                        .productId("35455")
                        .priority(1)
                        .amount(new Amount(25.45))
                        .currency(Currency.valueOf("EUR"))
                        .startDate(Util.parseStringToDateTime("2020-06-14-15.00.00"))
                        .endDate(Util.parseStringToDateTime("2020-06-14-18.30.00"))
                        .build(),

                Price.builder()
                        .brandId("1")
                        .priceList(3)
                        .productId("35455")
                        .priority(1)
                        .amount(new Amount(30.5))
                        .currency(Currency.valueOf("EUR"))
                        .startDate(Util.parseStringToDateTime("2020-06-15-00.00.00"))
                        .endDate(Util.parseStringToDateTime("2020-06-15-11.00.00"))
                        .build(),

                Price.builder()
                        .brandId("1")
                        .priceList(4)
                        .productId("35455")
                        .priority(1)
                        .amount(new Amount(38.95))
                        .currency(Currency.valueOf("EUR"))
                        .startDate(Util.parseStringToDateTime("2020-06-15-16.00.00"))
                        .endDate(Util.parseStringToDateTime("2020-12-30-23.59.59"))
                        .build()
        );
    }

}
