package com.example.prices;

import com.example.prices.infraestructure.rest.SearchPriceResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = PricesApplication.class)
public class SearchPriceIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void should_return_200_status_code_and_35_50_price_when_brandId_is_1_productId_is_35455_and_date_is_20200614_100000() {
        ResponseEntity<SearchPriceResponse> response = restTemplate.getForEntity(
                "/price?date=2020-06-14-10.00.00&product=35455&brand=1",
                SearchPriceResponse.class);

        assertEquals(response.getStatusCode().value(), 200);

        assertEquals("35455", response.getBody().getProductId());
        assertEquals("1", response.getBody().getBrandId());
        assertEquals("2020-06-14-00.00.00", response.getBody().getDateStart());
        assertEquals("2020-12-31-23.59.59", response.getBody().getDateEnd());
        assertEquals("1", response.getBody().getRate());
        assertEquals("35.50 EUR", response.getBody().getPrice());
    }

    @Test
    public void should_return_404_status_code_when_brandId_is_2_productId_is_35455_and_date_is_20200614_100000() {
        ResponseEntity<SearchPriceResponse> response = restTemplate.getForEntity(
                "/price?date=2020-06-14-10.00.00&product=35455&brand=2",
                SearchPriceResponse.class);

        assertEquals(response.getStatusCode().value(), 404);
    }

    @Test
    public void should_return_400_status_code_when_brandId_is_2_productId_is_35455_and_date_is_20200614() {
        ResponseEntity<SearchPriceResponse> response = restTemplate.getForEntity(
                "/price?date=2020-06-14-&product=35455&brand=1",
                SearchPriceResponse.class);

        assertEquals(response.getStatusCode().value(), 400);
    }

}
