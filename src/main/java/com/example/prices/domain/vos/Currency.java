package com.example.prices.domain.vos;

public enum Currency {

    EUR("EUR");

    public final String value;

    private Currency(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
