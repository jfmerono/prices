package com.example.prices.domain.vos;

import lombok.Getter;

public class Amount {

    @Getter
    private Double value;

    public Amount(Double value) {
        if(value < 0.0)
            throw new IllegalArgumentException("Price can not be negative.");
        this.value = value;
    }

}
