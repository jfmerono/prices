package com.example.prices.domain.services;

import com.example.prices.domain.aggregates.Price;
import com.example.prices.domain.exceptions.PriceNotFoundException;
import com.example.prices.domain.repositories.PriceRepositoryPort;

import java.time.LocalDateTime;
import java.util.Comparator;

public class PriceService {

    private final PriceRepositoryPort priceRepository;

    public PriceService(PriceRepositoryPort priceRepositoryPort) {
        this.priceRepository = priceRepositoryPort;
    }

    public Price searchPrice(LocalDateTime dateTime, String productId, String brandId) {
        return this.priceRepository.findAll().stream()
                .filter(p -> dateTime.isAfter(p.getStartDate()))
                .filter(p -> dateTime.isBefore(p.getEndDate()))
                .filter(p -> brandId.equals(p.getBrandId()))
                .filter(p -> productId.equals(p.getProductId()))
                .sorted(Comparator.comparing(Price::getPriority).reversed())
                .findFirst()
                .orElseThrow(() -> new PriceNotFoundException("Price not found"));
    }

}
