package com.example.prices.domain.repositories;

import com.example.prices.domain.aggregates.Price;

import java.util.List;

public interface PriceRepositoryPort {

    List<Price> findAll();

}
