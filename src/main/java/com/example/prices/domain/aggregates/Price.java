package com.example.prices.domain.aggregates;

import com.example.prices.domain.Util.Util;
import com.example.prices.domain.vos.Amount;
import com.example.prices.domain.vos.Currency;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Price {

    private final String productId;
    private final String brandId;
    private final Integer priceList;
    private final Integer priority;
    private final LocalDateTime startDate;
    private final LocalDateTime endDate;
    private Amount amount;
    private Currency currency;

    public String getPrice() {
        return Util.formatDoubleWithTwoDecimals(this.amount.getValue()) +  " " + this.currency.getValue();
    }

}
