package com.example.prices.domain.Util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Util {

    private Util() {}

    public static final String stringDateFormat = "yyyy-MM-dd-HH.mm.ss";

    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern(stringDateFormat);

    public static String parseDateTimeToString(LocalDateTime dateTime) {
        return dtf.format(dateTime);
    }

    public static LocalDateTime parseStringToDateTime(String dateTime) {
        return LocalDateTime.parse(dateTime, dtf);
    }

    public static String formatDoubleWithTwoDecimals(Double num) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        return new DecimalFormat("0.00", dfs).format(num);
    }

}
