package com.example.prices.infraestructure.config;

import com.example.prices.application.usecases.SearchPriceUseCase;
import com.example.prices.domain.repositories.PriceRepositoryPort;
import com.example.prices.domain.services.PriceService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public PriceService priceService(PriceRepositoryPort priceRepositoryPort) {
        return new PriceService(priceRepositoryPort);
    }

    @Bean
    public SearchPriceUseCase searchPriceUseCase(PriceService priceService) {
        return new SearchPriceUseCase(priceService);
    }

}
