package com.example.prices.infraestructure.rest;

import com.example.prices.application.usecases.SearchPriceRequest;
import com.example.prices.application.usecases.SearchPriceUseCase;
import com.example.prices.domain.Util.Util;
import com.example.prices.domain.exceptions.PriceNotFoundException;
import com.example.prices.infraestructure.rest.mapper.SearchPriceMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeParseException;

@RestController
@RequestMapping("/price")
public class PriceController {

    private final SearchPriceUseCase useCase;

    public PriceController(SearchPriceUseCase useCase) {
        this.useCase = useCase;
    }

    @GetMapping
    public ResponseEntity<SearchPriceResponse> filterPrice(@RequestParam String date,
                                                           @RequestParam String product,
                                                           @RequestParam String brand) {
        SearchPriceRequest request = SearchPriceRequest.builder()
                .date(Util.parseStringToDateTime(date))
                .productId(product)
                .brandId(brand)
                .build();

        SearchPriceResponse response =
                SearchPriceMapper.fromPriceToSearchPriceResponse(useCase.execute(request));

        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(PriceNotFoundException.class)
    public ResponseEntity priceNotFoundHandler(PriceNotFoundException e) {
        return new ResponseEntity(new Response("Price not found"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DateTimeParseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Response badDateFormat(DateTimeParseException e) {
        return new Response("Date is not formed right, please ensure format is " + Util.stringDateFormat);
    }

}
