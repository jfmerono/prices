package com.example.prices.infraestructure.rest.mapper;

import com.example.prices.infraestructure.rest.SearchPriceResponse;
import com.example.prices.domain.Util.Util;
import com.example.prices.domain.aggregates.Price;

public class SearchPriceMapper {

    private SearchPriceMapper() { }

    public static SearchPriceResponse fromPriceToSearchPriceResponse(Price price) {
        return SearchPriceResponse.builder()
                .brandId(price.getBrandId())
                .productId(price.getProductId())
                .dateStart(Util.parseDateTimeToString(price.getStartDate()))
                .dateEnd(Util.parseDateTimeToString(price.getEndDate()))
                .price(price.getPrice())
                .rate(String.valueOf(price.getPriceList()))
                .build();
    }

}
