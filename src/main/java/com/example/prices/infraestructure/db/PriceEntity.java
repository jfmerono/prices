package com.example.prices.infraestructure.db;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@Table(name="prices")
public class PriceEntity {

    @Id
    private Long priceId;
    private String productId;
    private String brandId;
    private String priceList;
    private Integer priority;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Double price;
    private String currency;
    private LocalDateTime lastUpdate;
    private String lastUpdateBy;

}
