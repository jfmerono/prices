package com.example.prices.infraestructure.db;

import com.example.prices.domain.aggregates.Price;
import com.example.prices.domain.repositories.PriceRepositoryPort;
import com.example.prices.infraestructure.db.mapper.PriceEntityMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PriceRepositoryAdapter implements PriceRepositoryPort {

    private final PriceJpaRepository priceJpaRepository;

    public PriceRepositoryAdapter(PriceJpaRepository priceJpaRepository) {
        this.priceJpaRepository = priceJpaRepository;
    }

    @Override
    public List<Price> findAll() {
        return this.priceJpaRepository.findAll().stream()
                .map(p -> PriceEntityMapper.fromEntityToDomain(p))
                .collect(Collectors.toList());
    }
}
