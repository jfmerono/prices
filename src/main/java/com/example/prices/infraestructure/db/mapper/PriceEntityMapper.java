package com.example.prices.infraestructure.db.mapper;

import com.example.prices.domain.aggregates.Price;
import com.example.prices.domain.vos.Amount;
import com.example.prices.domain.vos.Currency;
import com.example.prices.infraestructure.db.PriceEntity;

public class PriceEntityMapper {

    private PriceEntityMapper() { }

    public static Price fromEntityToDomain(PriceEntity e) {
        return Price.builder()
                .brandId(e.getBrandId())
                .productId(e.getProductId())
                .amount(new Amount(e.getPrice()))
                .currency(Currency.valueOf(e.getCurrency()))
                .startDate(e.getStartDate())
                .endDate(e.getEndDate())
                .priceList(Integer.valueOf(e.getPriceList()))
                .priority(e.getPriority())
                .build();
    }

}
