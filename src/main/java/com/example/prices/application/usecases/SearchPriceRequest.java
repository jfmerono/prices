package com.example.prices.application.usecases;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class SearchPriceRequest {

    private final LocalDateTime date;
    private final String productId;
    private final String brandId;

}
