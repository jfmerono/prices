package com.example.prices.application.usecases;

import com.example.prices.domain.aggregates.Price;
import com.example.prices.domain.services.PriceService;

public class SearchPriceUseCase {

    private final PriceService priceService;

    public SearchPriceUseCase(PriceService priceService) {
        this.priceService = priceService;
    }

    public Price execute(SearchPriceRequest request) {
        return priceService.searchPrice(request.getDate(), request.getProductId(), request.getBrandId());
    }

}
