-- public.prices definition

-- Drop table
DROP TABLE IF EXISTS public.prices;

CREATE TABLE public.prices (
	price float8 NULL,
	priority int4 NULL,
	end_date timestamp(6) NULL,
	last_update timestamp(6) NULL,
	price_id int8 NOT NULL,
	start_date timestamp(6) NULL,
	brand_id varchar(255) NULL,
	currency varchar(255) NULL,
	last_update_by varchar(255) NULL,
	price_list varchar(255) NULL,
	product_id varchar(255) NULL,
	CONSTRAINT prices_pkey PRIMARY KEY (price_id)
);