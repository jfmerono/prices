FROM maven:3.9.2-amazoncorretto-17 as build
WORKDIR /opt/app
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src src
RUN mvn package -DskipTests

FROM openjdk:17-alpine as production
WORKDIR /opt/app
ARG JAR_FILE=target/*.jar
COPY --from=build /opt/app/${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","app.jar"]